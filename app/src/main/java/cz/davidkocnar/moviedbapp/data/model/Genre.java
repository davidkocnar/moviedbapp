package cz.davidkocnar.moviedbapp.data.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Genre implements Parcelable
{

	@SerializedName("id")
	@Expose
	public Integer id;
	@SerializedName("name")
	@Expose
	public String name;
	public final static Parcelable.Creator<Genre> CREATOR = new Creator<Genre>() {

		@SuppressWarnings({"unchecked"})
		public Genre createFromParcel(Parcel in) {
			return new Genre(in);
		}

		public Genre[] newArray(int size) {
			return (new Genre[size]);
		}
	};

	private Genre(Parcel in) {
		this.id = ((Integer) in.readValue((Integer.class.getClassLoader())));
		this.name = ((String) in.readValue((String.class.getClassLoader())));
	}

	public Genre() {
	}

	@Override
	public String toString() {
		return "id:" + id + " name:" + name;
	}

	@Override
	public int hashCode() {
		return new StringBuilder().append(id).append(name).hashCode();
	}

	@Override
	public boolean equals(Object other) {
		if (other == this) {
			return true;
		}
		if (!(other instanceof Genre)) {
			return false;
		}
		Genre rhs = ((Genre) other);
		return id.equals(rhs.id);
	}

	public void writeToParcel(Parcel dest, int flags) {
		dest.writeValue(id);
		dest.writeValue(name);
	}

	public int describeContents() {
		return 0;
	}

}
