package cz.davidkocnar.moviedbapp.data.retrofit;

import cz.davidkocnar.moviedbapp.data.model.MovieDetailResponse;
import cz.davidkocnar.moviedbapp.data.model.MoviesResponse;
import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;


public interface ApiInterface {

	@GET("movie/changes")
	Observable<MoviesResponse> getMovies(@Query("api_key") String apiKey,
										 @Query("start_date") String startDate,
										 @Query("end_date") String endDate,
										 @Query("page") int page);

	@GET("movie/{movie_id}")
	Observable<MovieDetailResponse> getMovieDetail(@Path("movie_id") int movieId,
												   @Query("api_key") String apiKey);

}
