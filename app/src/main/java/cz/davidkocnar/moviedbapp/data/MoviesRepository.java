package cz.davidkocnar.moviedbapp.data;

import android.content.Context;

import cz.davidkocnar.moviedbapp.data.model.MovieDetailResponse;
import cz.davidkocnar.moviedbapp.data.model.MoviesResponse;
import cz.davidkocnar.moviedbapp.data.retrofit.ApiInterface;
import cz.davidkocnar.moviedbapp.data.retrofit.RetrofitClient;
import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;


public class MoviesRepository {

	private Context context;  // Application context
	private ApiInterface apiInterface;
	private static final String API_KEY = "54e1d1a73a6a9e535ac77aee83a02a86";

	public MoviesRepository(Context context) {
		this.context = context.getApplicationContext();
	}

	public Observable<MoviesResponse> requestMovies(String startDate, String endDate, int page) {
		if (apiInterface == null) apiInterface = RetrofitClient.getClient(context).create(ApiInterface.class);
		Observable<MoviesResponse> call = apiInterface.getMovies(API_KEY, startDate, endDate, page);

		Timber.d("# Movies list request: page=%d", page);

		return call.subscribeOn(Schedulers.newThread())
				.retry(2)
				.observeOn(Schedulers.io());
	}

	public Observable<MovieDetailResponse> requestMovieDetail(Integer id) {
		if (apiInterface == null) apiInterface = RetrofitClient.getClient(context).create(ApiInterface.class);
		Observable<MovieDetailResponse> call = apiInterface.getMovieDetail(id, API_KEY);

		Timber.d("# Movie detail request for id=%d", id);

		return call.subscribeOn(Schedulers.computation())
				.retry(4);
	}

}
