package cz.davidkocnar.moviedbapp.data.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MoviesResponse implements Parcelable {

	@SerializedName("results")
	@Expose
	private List<MovieDetailResponse> results = null;
	@SerializedName("page")
	@Expose
	private Integer page;
	@SerializedName("total_pages")
	@Expose
	private Integer totalPages;
	@SerializedName("total_results")
	@Expose
	private Integer totalResults;
	public final static Parcelable.Creator<MoviesResponse> CREATOR = new Creator<MoviesResponse>() {

		@SuppressWarnings({
				"unchecked"
		})
		public MoviesResponse createFromParcel(Parcel in) {
			return new MoviesResponse(in);
		}

		public MoviesResponse[] newArray(int size) {
			return (new MoviesResponse[size]);
		}

	};

	private MoviesResponse(Parcel in) {
		in.readList(this.results, (MovieDetailResponse.class.getClassLoader()));
		this.page = ((Integer) in.readValue((Integer.class.getClassLoader())));
		this.totalPages = ((Integer) in.readValue((Integer.class.getClassLoader())));
		this.totalResults = ((Integer) in.readValue((Integer.class.getClassLoader())));
	}

	public MoviesResponse() {}

	public List<MovieDetailResponse> getResults() {
		return results;
	}

	public void setResults(List<MovieDetailResponse> results) {
		this.results = results;
	}

	public Integer getPage() {
		return page;
	}

	public void setPage(Integer page) {
		this.page = page;
	}

	public Integer getTotalPages() {
		return totalPages;
	}

	public void setTotalPages(Integer totalPages) {
		this.totalPages = totalPages;
	}

	public Integer getTotalResults() {
		return totalResults;
	}

	public void setTotalResults(Integer totalResults) {
		this.totalResults = totalResults;
	}

	@Override
	public String toString() {
		return "results=" + results + " page" + page + " totalPages" + totalPages + " totalResults" + totalResults;
	}

	@Override
	public int hashCode() {
		return new StringBuilder().append(results).append(totalResults).append(page).append(totalPages).hashCode();
	}

	@Override
	public boolean equals(Object other) {
		if (other == this) {
			return true;
		}
		if (!(other instanceof MoviesResponse)) {
			return false;
		}
		MoviesResponse rhs = ((MoviesResponse) other);
		return results.equals(rhs.results) &&
				totalResults.equals(rhs.totalResults) &&
				page.equals(rhs.page) &&
				totalPages.equals(rhs.totalPages);
	}

	public void writeToParcel(Parcel dest, int flags) {
		dest.writeList(results);
		dest.writeValue(page);
		dest.writeValue(totalPages);
		dest.writeValue(totalResults);
	}

	public int describeContents() {
		return 0;
	}

}
