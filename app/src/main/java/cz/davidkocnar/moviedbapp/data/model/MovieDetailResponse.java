package cz.davidkocnar.moviedbapp.data.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class MovieDetailResponse implements Parcelable {

	@SerializedName("adult")
	@Expose
	private Boolean adult;
	@SerializedName("backdrop_path")
	@Expose
	private Object backdropPath;
	@SerializedName("belongs_to_collection")
	@Expose
	private Object belongsToCollection;
	@SerializedName("budget")
	@Expose
	private int budget = 0;
	@SerializedName("genres")
	@Expose
	public List<Genre> genres = new ArrayList<>();
	@SerializedName("homepage")
	@Expose
	private Object homepage;
	@SerializedName("id")
	@Expose
	public int id;
	@SerializedName("imdb_id")
	@Expose
	private String imdbId;
	@SerializedName("original_language")
	@Expose
	public String originalLanguage;
	@SerializedName("original_title")
	@Expose
	public String originalTitle;
	@SerializedName("overview")
	@Expose
	public String overview;
	@SerializedName("popularity")
	@Expose
	private Double popularity;
	@SerializedName("poster_path")
	@Expose
	private String posterPath;
	@SerializedName("release_date")
	@Expose
	public String releaseDate;
	@SerializedName("revenue")
	@Expose
	private int revenue;
	@SerializedName("runtime")
	@Expose
	private Integer runtime;
	@SerializedName("status")
	@Expose
	private String status;
	@SerializedName("tagline")
	@Expose
	public String tagline;
	@SerializedName("title")
	@Expose
	public String title;
	@SerializedName("video")
	@Expose
	private Boolean video;
	@SerializedName("vote_average")
	@Expose
	public Float voteAverage;
	@SerializedName("vote_count")
	@Expose
	public int voteCount;

	//@SerializedName("spoken_languages")
	//@Expose
	//public List<SpokenLanguage> spokenLanguages = null;
	//@SerializedName("production_companies")
	//@Expose
	//public List<ProductionCompany> productionCompanies = null;
	//@SerializedName("production_countries")
	//@Expose
	//public List<ProductionCountry> productionCountries = null;

	public boolean dataError = false;

	private final static String POSTER_BASE_URL = "https://image.tmdb.org/t/p/w185_and_h278_bestv2";

	public final static Parcelable.Creator<MovieDetailResponse> CREATOR = new Creator<MovieDetailResponse>() {

		@SuppressWarnings({"unchecked"})
		public MovieDetailResponse createFromParcel(Parcel in) {
			return new MovieDetailResponse(in);
		}

		public MovieDetailResponse[] newArray(int size) {
			return (new MovieDetailResponse[size]);
		}
	};

	private MovieDetailResponse(Parcel in) {
		this.adult = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
		this.backdropPath = in.readValue((Object.class.getClassLoader()));
		this.belongsToCollection = in.readValue((Object.class.getClassLoader()));
		this.budget = ((Integer) in.readValue((Integer.class.getClassLoader())));
		in.readList(this.genres, (cz.davidkocnar.moviedbapp.data.model.Genre.class.getClassLoader()));
		this.homepage = in.readValue((Object.class.getClassLoader()));
		this.id = ((Integer) in.readValue((Integer.class.getClassLoader())));
		this.imdbId = ((String) in.readValue((String.class.getClassLoader())));
		this.originalLanguage = ((String) in.readValue((String.class.getClassLoader())));
		this.originalTitle = ((String) in.readValue((String.class.getClassLoader())));
		this.overview = ((String) in.readValue((String.class.getClassLoader())));
		this.popularity = ((Double) in.readValue((Double.class.getClassLoader())));
		this.posterPath = ((String) in.readValue((String.class.getClassLoader())));
		this.releaseDate = ((String) in.readValue((String.class.getClassLoader())));
		this.revenue = ((Integer) in.readValue((Integer.class.getClassLoader())));
		this.runtime = ((Integer) in.readValue((Integer.class.getClassLoader())));
		this.status = ((String) in.readValue((String.class.getClassLoader())));
		this.tagline = ((String) in.readValue((String.class.getClassLoader())));
		this.title = ((String) in.readValue((String.class.getClassLoader())));
		this.video = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
		this.voteAverage = ((Float) in.readValue((Float.class.getClassLoader())));
		this.voteCount = ((Integer) in.readValue((Integer.class.getClassLoader())));
	}

	public MovieDetailResponse() {
	}

	public String getMoviePosterUrl() {
		if (this.posterPath == null) return null;
		return POSTER_BASE_URL + this.posterPath;
	}

	public String getGenresPreparedString() {
		StringBuilder sb = new StringBuilder();
		for (Genre genre : this.genres) {
			if (sb.length() > 0) sb.append(", ");
			sb.append(genre.name);
		}
		return sb.toString();
	}

	@Override
	public String toString() {
		return "id:" + id + ", originalTitle:" + originalTitle + ", title:" + title;
	}

	@Override
	public int hashCode() {
		return new StringBuilder().append(id).hashCode();
	}

	@Override
	public boolean equals(Object other) {
		if (other == this) {
			return true;
		}
		if (!(other instanceof MovieDetailResponse)) {
			return false;
		}
		MovieDetailResponse rhs = ((MovieDetailResponse) other);
		return id == rhs.id;
	}

	public void writeToParcel(Parcel dest, int flags) {
		dest.writeValue(adult);
		dest.writeValue(backdropPath);
		dest.writeValue(belongsToCollection);
		dest.writeValue(budget);
		dest.writeList(genres);
		dest.writeValue(homepage);
		dest.writeValue(id);
		dest.writeValue(imdbId);
		dest.writeValue(originalLanguage);
		dest.writeValue(originalTitle);
		dest.writeValue(overview);
		dest.writeValue(popularity);
		dest.writeValue(posterPath);
		dest.writeValue(releaseDate);
		dest.writeValue(revenue);
		dest.writeValue(runtime);
		dest.writeValue(status);
		dest.writeValue(tagline);
		dest.writeValue(title);
		dest.writeValue(video);
		dest.writeValue(voteAverage);
		dest.writeValue(voteCount);
	}

	public int describeContents() {
		return 0;
	}

}
