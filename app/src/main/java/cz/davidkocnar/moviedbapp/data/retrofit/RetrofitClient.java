package cz.davidkocnar.moviedbapp.data.retrofit;

import android.content.Context;
import android.os.SystemClock;

import com.blankj.utilcode.util.NetworkUtils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.File;

import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;


public class RetrofitClient {

	private static final String BASE_URL = "https://api.themoviedb.org/3/";
	private static final int CACHE_SIZE = 10485760;  // 10 * 1024 * 1024 = 10 MB
	private static final int MAX_AGE_ONLINE = 3600;  // 1 h
	private static final int MAX_STALE_OFFLINE = 86400;  // 60 * 60 * 24 = 1 day
	public static final int RATE_LIMITING = 300;
	private static Retrofit retrofit = null;

	public static Retrofit getClient(Context context) {
		if (retrofit == null) {

			OkHttpClient okHttpClient = new OkHttpClient.Builder()
					.cache(new Cache(new File(context.getCacheDir(), "apiResponses"), CACHE_SIZE)) // max 10 MB cache
					.addInterceptor(chain -> {
						Request request = chain.request();
						if (NetworkUtils.isConnected()) {  // When network is active
							request = request.newBuilder().header("Cache-Control", "public, max-age=" + MAX_AGE_ONLINE).build();
						} else {  // When network is inactive, offline mode
							request = request.newBuilder().header("Cache-Control", "public, only-if-cached, max-stale=" + MAX_STALE_OFFLINE).build();
						}
						SystemClock.sleep(RATE_LIMITING);
						return chain.proceed(request);
					})
					.build();

			Gson gson = new GsonBuilder()
					.setLenient()  // For incorrect JSON format
					.create();

			retrofit = new Retrofit.Builder()
					.baseUrl(BASE_URL)
					.client(okHttpClient)
					.addCallAdapterFactory(RxJava2CallAdapterFactory.create())
					.addConverterFactory(GsonConverterFactory.create(gson))
					.build();
		}
		return retrofit;
	}

}