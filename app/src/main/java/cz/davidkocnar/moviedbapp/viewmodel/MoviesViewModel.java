package cz.davidkocnar.moviedbapp.viewmodel;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import cz.davidkocnar.moviedbapp.data.MoviesRepository;
import cz.davidkocnar.moviedbapp.data.model.MovieDetailResponse;
import cz.davidkocnar.moviedbapp.data.model.MoviesResponse;
import cz.davidkocnar.moviedbapp.data.retrofit.RetrofitClient;
import cz.kinst.jakub.view.SimpleStatefulLayout;
import io.reactivex.BackpressureStrategy;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import timber.log.Timber;


public class MoviesViewModel extends ViewModel {

	private MoviesRepository repository;
	public MutableLiveData<List<MovieDetailResponse>> moviesListResponse = new MutableLiveData<>();  // List of all movies data (only IDs from the start)
	public MutableLiveData<List<MovieDetailResponse>> moviesFullResponse = new MutableLiveData<>();  // Received one (full) movie data to reload adapter
	public MutableLiveData<String> state = new MutableLiveData<>();  // Entire data state for activity
	private CompositeDisposable compositeDisposable = new CompositeDisposable();

	private int lastLoadedPage = 0;
	private Integer numberOfDays = null;
	private boolean loadingNextPage = false;

	private static final String DATE_PATTERN = "yyyy-MM-dd";
	private SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_PATTERN, Locale.ENGLISH);

	public RecyclerView.OnScrollListener scrollListener = new RecyclerView.OnScrollListener() {
		@Override
		public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
			LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
			if (layoutManager != null && !loadingNextPage) {
				int visibleItemCount = recyclerView.getChildCount();
				int totalItemCount = layoutManager.getItemCount();
				int pastVisiblesItems = layoutManager.findLastVisibleItemPosition();

				if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
					loadingNextPage = true;

					createRequest(++lastLoadedPage);
				}
			}
		}
	};

	public View.OnClickListener statefulRetryListener = view -> initData(numberOfDays);

	MoviesViewModel(MoviesRepository repo) {
		this.repository = repo;
	}

	public void initData(Integer numberOfDays) {
		if(numberOfDays == null) numberOfDays = 1;
		if(!numberOfDays.equals(this.numberOfDays)) {
			moviesListResponse.setValue(new ArrayList<>());
			this.lastLoadedPage = 0;
			this.numberOfDays = numberOfDays;
		}

		if(this.lastLoadedPage == 0) {
			state.setValue(SimpleStatefulLayout.State.PROGRESS);
			createRequest(1);
		}
		else {
			useLoadedData();
		}
	}

	private void createRequest(int page) {
		Calendar cal = Calendar.getInstance();
		String today = dateFormat.format(cal.getTime());
		cal.add(Calendar.DATE, -this.numberOfDays);
		String startDate = dateFormat.format(cal.getTime());
		compositeDisposable.add(repository.requestMovies(startDate, today, page)
				.flatMap(response -> Observable
						.fromIterable(response.getResults())
						.filter(it -> it.id != 0)
						.toList()
						.toObservable()
						.doOnNext(list -> setNewPageData(response, list)))
				.doOnError(this::setDataError)
				.flatMap(Observable::fromIterable)
				.concatMap(item -> repository.requestMovieDetail(item.id)
						.doOnError(this::setDataError)
						.onErrorResumeNext(Observable.just(item)))
				.toFlowable(BackpressureStrategy.BUFFER)
				.delay(RetrofitClient.RATE_LIMITING, TimeUnit.MILLISECONDS)
				.observeOn(AndroidSchedulers.mainThread())
				.subscribe(this::fillDetailData, this::setDataError));
	}

	private synchronized void fillDetailData(MovieDetailResponse response) {
		List<MovieDetailResponse> movies = this.moviesListResponse.getValue();
		int index = -1;
		if (movies != null) {
			index = movies.indexOf(response);
		}
		if (index != -1) {
			movies.set(index, response);
			this.moviesListResponse.postValue(movies);

			List<MovieDetailResponse> originalFullMovies = this.moviesFullResponse.getValue();
			if (originalFullMovies == null) {
				this.moviesFullResponse.postValue(new ArrayList<>(Collections.singletonList(response)));
			}
			else {
				originalFullMovies.add(response);
				this.moviesFullResponse.postValue(originalFullMovies);
			}
		}
	}

	private void useLoadedData() {
		this.moviesListResponse.postValue(this.moviesListResponse.getValue());
		this.state.postValue(SimpleStatefulLayout.State.CONTENT);
	}

	private void setNewPageData(MoviesResponse response, List<MovieDetailResponse> list) {
		this.lastLoadedPage = response.getPage();
		this.loadingNextPage = false;
		if(this.lastLoadedPage == 1 && list.isEmpty()) {
			this.state.postValue(SimpleStatefulLayout.State.EMPTY);
		}
		else {
			List<MovieDetailResponse> moviesData = this.moviesListResponse.getValue();
			if (moviesData == null) {
				this.moviesListResponse.postValue(list);
			}
			else {
				moviesData.addAll(list);
				this.moviesListResponse.postValue(moviesData);
			}
			this.state.postValue(SimpleStatefulLayout.State.CONTENT);
		}
	}



	private void setDataError(Throwable throwable) {
		Timber.e("Request error! %s", throwable.getMessage());
		if (this.moviesListResponse.getValue() == null || this.moviesListResponse.getValue().isEmpty()) {
			this.state.postValue(SimpleStatefulLayout.State.OFFLINE);
		}
	}

	public void retryDetailRequest(int movieId) {
		compositeDisposable.add(repository.requestMovieDetail(movieId)
				.subscribe(this::fillDetailData, this::setDataError));
	}

	@Override
	protected void onCleared() {
		super.onCleared();
		compositeDisposable.dispose();
	}

	public static class Factory implements ViewModelProvider.Factory {
		private final MoviesRepository repository;

		public Factory(MoviesRepository repository) {
			this.repository = repository;
		}

		@NonNull
		@Override
		@SuppressWarnings("unchecked")
		public MoviesViewModel create(@NonNull Class modelClass) {
			return new MoviesViewModel(repository);
		}
	}
}
