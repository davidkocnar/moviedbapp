package cz.davidkocnar.moviedbapp.ui;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import cz.davidkocnar.moviedbapp.R;
import cz.davidkocnar.moviedbapp.data.model.MovieDetailResponse;
import cz.davidkocnar.moviedbapp.databinding.ActivityMovieDetailBinding;


public class MovieDetailActivity extends AppCompatActivity {

	public static final String POSTER_IMAGE_TRANS_NAME = "poster_image";
	public static final String MOVIE_ITEM_BUNDLE = "MOVIE_ITEM_BUNDLE";
	public static final int REQUEST_OPEN_DETAIL = 1;

	private ActivityMovieDetailBinding binding;
	private MovieDetailResponse movie;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.binding = DataBindingUtil.setContentView(this, R.layout.activity_movie_detail);
		Toolbar toolbar = findViewById(R.id.detail_toolbar);
		setSupportActionBar(toolbar);
		supportPostponeEnterTransition();

		ActionBar actionBar = getSupportActionBar();
		if (actionBar != null) {
			actionBar.setDisplayHomeAsUpEnabled(true);
		}

		if (savedInstanceState == null) {
			movie = getIntent().getParcelableExtra(MOVIE_ITEM_BUNDLE);
		}
		else {
			movie = savedInstanceState.getParcelable(MOVIE_ITEM_BUNDLE);
		}

		if (movie != null) {
			setTitle(movie.title);
			binding.setMovie(movie);

			if (movie.getMoviePosterUrl() == null) {
				startEnterTransition();
			}
			else {
				Picasso.get()
						.load(movie.getMoviePosterUrl())
						.fit()
						.placeholder(R.drawable.ic_movie)
						.centerCrop()
						.into(binding.posterImageBig, new Callback() {
							@Override
							public void onSuccess() {
								startEnterTransition();
							}

							@Override
							public void onError(Exception e) {
								startEnterTransition();
							}
						});
			}
		}
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putParcelable(MOVIE_ITEM_BUNDLE, movie);
	}

	private void startEnterTransition() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
			binding.posterImageBig.setTransitionName(POSTER_IMAGE_TRANS_NAME);
			startPostponedEnterTransition();
		}
	}

	@Override
	public void onBackPressed() {
		supportFinishAfterTransition();
	}

	@Override
	@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
	public void finishAfterTransition() {
		Intent intent = new Intent();
		intent.putExtra(MOVIE_ITEM_BUNDLE, movie);
		setResult(RESULT_OK, intent);

		super.finishAfterTransition();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == android.R.id.home) {
			onBackPressed();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
