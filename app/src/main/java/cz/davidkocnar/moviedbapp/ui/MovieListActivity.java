package cz.davidkocnar.moviedbapp.ui;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import com.blankj.utilcode.util.Utils;

import java.util.ArrayList;
import java.util.List;

import cz.davidkocnar.moviedbapp.BuildConfig;
import cz.davidkocnar.moviedbapp.R;
import cz.davidkocnar.moviedbapp.data.MoviesRepository;
import cz.davidkocnar.moviedbapp.data.model.MovieDetailResponse;
import cz.davidkocnar.moviedbapp.databinding.ActivityMovieListBinding;
import cz.davidkocnar.moviedbapp.viewmodel.MoviesViewModel;
import timber.log.Timber;


public class MovieListActivity extends AppCompatActivity {

	private MoviesViewModel viewModel;
	private MoviesRecyclerAdapter adapter;
	private ActivityMovieListBinding binding;

	View.OnClickListener editTextClickListener = v -> {
		if (v.getId() == binding.filterEditText.getId()) {
			binding.filterEditText.setCursorVisible(true);
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		binding = DataBindingUtil.setContentView(this, R.layout.activity_movie_list);
		Utils.init(this.getApplication());
		if(BuildConfig.DEBUG) {
			Timber.plant(new Timber.DebugTree());
		}

		Toolbar toolbar = findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);
		toolbar.setTitle(getTitle());

		initViewModel();
		setupRecyclerView(binding.movieList.moviesRecyclerView);

		binding.filterEditText.setOnClickListener(editTextClickListener);
		binding.filterEditText.setOnEditorActionListener(editorActionListener);
		binding.movieList.statefulView.setOfflineRetryOnClickListener(viewModel.statefulRetryListener);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		this.adapter.destroyActivity();
	}

	public void initViewModel() {
		MoviesRepository repository = new MoviesRepository(this.getApplicationContext());
		this.viewModel = ViewModelProviders
				.of(this, new MoviesViewModel.Factory(repository))
				.get(MoviesViewModel.class);
		viewModel.state.observe(this, this::onStateChanged);
		viewModel.moviesListResponse.observe(this, this::setAdapterData);
		viewModel.moviesFullResponse.observe(this, this::fillInAdapterData);
		viewModel.initData(null);
	}

	private void setupRecyclerView(@NonNull RecyclerView recyclerView) {
		this.adapter = new MoviesRecyclerAdapter(this, this.viewModel, null);
		recyclerView.setItemAnimator(new DefaultItemAnimator());
		recyclerView.setAdapter(this.adapter);
		recyclerView.addOnScrollListener(this.viewModel.scrollListener);
	}

	private synchronized void fillInAdapterData(List<MovieDetailResponse> moviesFullData) {
		if (moviesFullData != null) {
			List<MovieDetailResponse> copyList = new ArrayList<>(moviesFullData);
			for (MovieDetailResponse movie : copyList) {
				Timber.i("Filling in data of movie=%s", movie);
				this.adapter.fillData(movie);
				if (this.viewModel.moviesFullResponse.getValue() != null) {
					this.viewModel.moviesFullResponse.getValue().remove(movie);
				}
			}
		}
	}

	private void setAdapterData(List<MovieDetailResponse> moviesResponse) {
		int originalSize = this.adapter.getItemCount();
		if (moviesResponse == null || moviesResponse.isEmpty()) {
			this.adapter.removeAll();
		}
		else if (originalSize != moviesResponse.size()) {
			this.adapter.addItems(moviesResponse.subList(originalSize, moviesResponse.size()));
			runOnUiThread(() -> this.adapter.notifyItemRangeInserted(originalSize, moviesResponse.size()-originalSize));
		}
	}

	private void onStateChanged(String s) {
		binding.movieList.statefulView.setState(s);
	}

	TextView.OnEditorActionListener editorActionListener = new TextView.OnEditorActionListener() {
		@Override
		public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
			binding.filterEditText.setCursorVisible(false);
			if (actionId == EditorInfo.IME_ACTION_DONE) {
				InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				if (in != null) {
					in.hideSoftInputFromWindow(binding.filterEditText.getApplicationWindowToken(),InputMethodManager.HIDE_NOT_ALWAYS);
				}
				String filterText = binding.filterEditText.getEditableText().toString();
				viewModel.initData(filterText.isEmpty()? null: Integer.parseInt(filterText));
				return true;
			}
			return false;
		}
	};
}
