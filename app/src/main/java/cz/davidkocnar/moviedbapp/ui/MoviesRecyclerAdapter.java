package cz.davidkocnar.moviedbapp.ui;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import cz.davidkocnar.moviedbapp.R;
import cz.davidkocnar.moviedbapp.data.model.MovieDetailResponse;
import cz.davidkocnar.moviedbapp.databinding.MovieListContentBinding;
import cz.davidkocnar.moviedbapp.viewmodel.MoviesViewModel;
import timber.log.Timber;


public class MoviesRecyclerAdapter extends RecyclerView.Adapter<MoviesRecyclerAdapter.ViewHolder> {

	private Activity activity;
	private List<MovieDetailResponse> items = new ArrayList<>();
	private Picasso picasso = Picasso.get();

	private final View.OnClickListener mOnClickListener = view -> {
		MovieDetailResponse item = (MovieDetailResponse) view.getTag();
		Context context = view.getContext();
		Intent intent = new Intent(context, MovieDetailActivity.class);
		intent.putExtra(MovieDetailActivity.MOVIE_ITEM_BUNDLE, item);
		intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP && this.activity != null) {
			View navigationBar = activity.getWindow().getDecorView().findViewById(android.R.id.navigationBarBackground);
			View statusBar = activity.getWindow().getDecorView().findViewById(android.R.id.statusBarBackground);
			ImageView posterImage = view.findViewById(R.id.poster_image);
			posterImage.setTransitionName(MovieDetailActivity.POSTER_IMAGE_TRANS_NAME);
			ActivityOptions options;

			if (navigationBar != null && statusBar != null) {
				options = ActivityOptions.makeSceneTransitionAnimation(this.activity,
						Pair.create(statusBar, Window.STATUS_BAR_BACKGROUND_TRANSITION_NAME),
						Pair.create(navigationBar, Window.NAVIGATION_BAR_BACKGROUND_TRANSITION_NAME),
						Pair.create(posterImage, MovieDetailActivity.POSTER_IMAGE_TRANS_NAME));
			}
			else {
				options = ActivityOptions.makeSceneTransitionAnimation(this.activity,
						Pair.create(posterImage, MovieDetailActivity.POSTER_IMAGE_TRANS_NAME));
			}
			activity.startActivityForResult(intent, MovieDetailActivity.REQUEST_OPEN_DETAIL, options.toBundle());
			this.activity.overridePendingTransition(0,0);
		}
		else {
			context.startActivity(intent);
		}
	};
	private MoviesViewModel viewModel;

	MoviesRecyclerAdapter(Activity activity, MoviesViewModel viewModel, List<MovieDetailResponse> items) {
		this.activity = activity;
		this.viewModel = viewModel;
		if(items != null) {
			this.items = items;
		}
	}

	@NonNull
	@Override
	public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
		MovieListContentBinding binding = DataBindingUtil.inflate(
				LayoutInflater.from(parent.getContext()),
				R.layout.movie_list_content, parent, false);

		return new ViewHolder(binding);
	}

	@Override
	public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
		MovieDetailResponse movie = items.get(position);
		holder.binding.setMovie(movie);
		holder.itemView.setTag(movie);

		picasso.load(movie.getMoviePosterUrl())
				.placeholder(R.drawable.ic_movie)
				.fit()
				.centerCrop()
				.into(holder.binding.posterImage);

		if (movie.title != null) {
			holder.itemView.setOnClickListener(mOnClickListener);
			holder.binding.itemStatefulView.showContent();
		}
		else {
			holder.itemView.setOnClickListener(null);
			if (movie.dataError) {
				holder.binding.itemStatefulView.showOffline();
				holder.binding.itemStatefulView.setOfflineRetryOnClickListener(
						v -> viewModel.retryDetailRequest(movie.id));
			}
			else {
				holder.binding.itemStatefulView.showProgress();
			}
		}
	}

	@Override
	public int getItemCount() {
		return items==null? 0 : items.size();
	}

	public void addItems(List<MovieDetailResponse> results) {
		this.items.addAll(results);
	}

	public void fillData(MovieDetailResponse movieDetailResponse) {
		int position = items.indexOf(movieDetailResponse);
		if (position == -1) {
			Timber.e("Position not found for Movie %s", movieDetailResponse);
		}
		else if (movieDetailResponse.title == null) {
			items.get(position).dataError = true;
			notifyItemChanged(position);
		}
		else {
			items.set(position, movieDetailResponse);
			notifyItemChanged(position);
		}
	}

	public void destroyActivity() {
		this.activity = null;
	}

	public void removeAll() {
		int originalSize = getItemCount();
		this.items = new ArrayList<>();
		notifyItemRangeRemoved(0, originalSize);
	}

	class ViewHolder extends RecyclerView.ViewHolder {
		private final MovieListContentBinding binding;

		ViewHolder(MovieListContentBinding binding) {
			super(binding.getRoot());
			this.binding = binding;
		}
	}
}
